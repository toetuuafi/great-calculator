﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.NumbBox = new System.Windows.Forms.TextBox();
            this.BtnMultiply = new System.Windows.Forms.Button();
            this.BtnMinus = new System.Windows.Forms.Button();
            this.BtnDivide = new System.Windows.Forms.Button();
            this.BtnPlus = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.Square = new System.Windows.Forms.Button();
            this.Trigbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // NumbBox
            // 
            this.NumbBox.Location = new System.Drawing.Point(12, 12);
            this.NumbBox.Name = "NumbBox";
            this.NumbBox.Size = new System.Drawing.Size(246, 20);
            this.NumbBox.TabIndex = 2;
            // 
            // BtnMultiply
            // 
            this.BtnMultiply.Location = new System.Drawing.Point(12, 51);
            this.BtnMultiply.Name = "BtnMultiply";
            this.BtnMultiply.Size = new System.Drawing.Size(75, 23);
            this.BtnMultiply.TabIndex = 3;
            this.BtnMultiply.Text = "*";
            this.BtnMultiply.UseVisualStyleBackColor = true;
            this.BtnMultiply.Click += new System.EventHandler(this.BtnMultiply_Click);
            // 
            // BtnMinus
            // 
            this.BtnMinus.Location = new System.Drawing.Point(12, 109);
            this.BtnMinus.Name = "BtnMinus";
            this.BtnMinus.Size = new System.Drawing.Size(75, 23);
            this.BtnMinus.TabIndex = 4;
            this.BtnMinus.Text = "-";
            this.BtnMinus.UseVisualStyleBackColor = true;
            // 
            // BtnDivide
            // 
            this.BtnDivide.Location = new System.Drawing.Point(12, 80);
            this.BtnDivide.Name = "BtnDivide";
            this.BtnDivide.Size = new System.Drawing.Size(75, 23);
            this.BtnDivide.TabIndex = 5;
            this.BtnDivide.Text = "/";
            this.BtnDivide.UseVisualStyleBackColor = true;
            // 
            // BtnPlus
            // 
            this.BtnPlus.Location = new System.Drawing.Point(12, 138);
            this.BtnPlus.Name = "BtnPlus";
            this.BtnPlus.Size = new System.Drawing.Size(75, 23);
            this.BtnPlus.TabIndex = 6;
            this.BtnPlus.Text = "+";
            this.BtnPlus.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(183, 138);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 7;
            this.button5.Text = "=";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // Square
            // 
            this.Square.Location = new System.Drawing.Point(93, 51);
            this.Square.Name = "Square";
            this.Square.Size = new System.Drawing.Size(75, 23);
            this.Square.TabIndex = 8;
            this.Square.Text = "^2";
            this.Square.UseVisualStyleBackColor = true;
            this.Square.Click += new System.EventHandler(this.Square_Click);
            // 
            // Trigbtn
            // 
            this.Trigbtn.Location = new System.Drawing.Point(183, 109);
            this.Trigbtn.Name = "Trigbtn";
            this.Trigbtn.Size = new System.Drawing.Size(75, 23);
            this.Trigbtn.TabIndex = 10;
            this.Trigbtn.Text = "Trig Clac";
            this.Trigbtn.UseVisualStyleBackColor = true;
            this.Trigbtn.Click += new System.EventHandler(this.Trigbtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 457);
            this.Controls.Add(this.Trigbtn);
            this.Controls.Add(this.Square);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.BtnPlus);
            this.Controls.Add(this.BtnDivide);
            this.Controls.Add(this.BtnMinus);
            this.Controls.Add(this.BtnMultiply);
            this.Controls.Add(this.NumbBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.TextBox NumbBox;
        private System.Windows.Forms.Button BtnMultiply;
        private System.Windows.Forms.Button BtnMinus;
        private System.Windows.Forms.Button BtnDivide;
        private System.Windows.Forms.Button BtnPlus;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button Square;
        private System.Windows.Forms.Button Trigbtn;
    }
}

