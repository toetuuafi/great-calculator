﻿namespace WindowsFormsApp1
{
    partial class TrigCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumbBox = new System.Windows.Forms.TextBox();
            this.BtnSin = new System.Windows.Forms.Button();
            this.BtnTan = new System.Windows.Forms.Button();
            this.BtnCos = new System.Windows.Forms.Button();
            this.BtnMult = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NumbBox
            // 
            this.NumbBox.Location = new System.Drawing.Point(13, 12);
            this.NumbBox.Name = "NumbBox";
            this.NumbBox.Size = new System.Drawing.Size(237, 20);
            this.NumbBox.TabIndex = 0;
            // 
            // BtnSin
            // 
            this.BtnSin.Location = new System.Drawing.Point(13, 39);
            this.BtnSin.Name = "BtnSin";
            this.BtnSin.Size = new System.Drawing.Size(75, 23);
            this.BtnSin.TabIndex = 1;
            this.BtnSin.Text = "Sin()";
            this.BtnSin.UseVisualStyleBackColor = true;
            this.BtnSin.Click += new System.EventHandler(this.BtnSin_Click);
            // 
            // BtnTan
            // 
            this.BtnTan.Location = new System.Drawing.Point(175, 39);
            this.BtnTan.Name = "BtnTan";
            this.BtnTan.Size = new System.Drawing.Size(75, 23);
            this.BtnTan.TabIndex = 2;
            this.BtnTan.Text = "Tan()";
            this.BtnTan.UseVisualStyleBackColor = true;
            this.BtnTan.Click += new System.EventHandler(this.BtnTan_Click);
            // 
            // BtnCos
            // 
            this.BtnCos.Location = new System.Drawing.Point(94, 39);
            this.BtnCos.Name = "BtnCos";
            this.BtnCos.Size = new System.Drawing.Size(75, 23);
            this.BtnCos.TabIndex = 3;
            this.BtnCos.Text = "Cos()";
            this.BtnCos.UseVisualStyleBackColor = true;
            this.BtnCos.Click += new System.EventHandler(this.BtnCos_Click);
            // 
            // BtnMult
            // 
            this.BtnMult.Location = new System.Drawing.Point(13, 68);
            this.BtnMult.Name = "BtnMult";
            this.BtnMult.Size = new System.Drawing.Size(75, 23);
            this.BtnMult.TabIndex = 4;
            this.BtnMult.Text = "*";
            this.BtnMult.UseVisualStyleBackColor = true;
            this.BtnMult.Click += new System.EventHandler(this.BtnMult_Click);
            // 
            // TrigCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 465);
            this.Controls.Add(this.BtnMult);
            this.Controls.Add(this.BtnCos);
            this.Controls.Add(this.BtnTan);
            this.Controls.Add(this.BtnSin);
            this.Controls.Add(this.NumbBox);
            this.Name = "TrigCalc";
            this.Text = "ScientificCalculator";
            this.Load += new System.EventHandler(this.ScientificCalculator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NumbBox;
        private System.Windows.Forms.Button BtnSin;
        private System.Windows.Forms.Button BtnTan;
        private System.Windows.Forms.Button BtnCos;
        private System.Windows.Forms.Button BtnMult;
    }
}