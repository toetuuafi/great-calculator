﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class TrigCalc : Form
    {
        static float ans;
        public TrigCalc()
        {
            InitializeComponent();
        }

        private void ScientificCalculator_Load(object sender, EventArgs e)
        {

        }

        private void BtnSin_Click(object sender, EventArgs e)
        {
            var i = float.Parse(NumbBox.Text);
            NumbBox.Text = "" + Math.Sin(i);
        }

        private void BtnCos_Click(object sender, EventArgs e)
        {
            var i = float.Parse(NumbBox.Text);
            NumbBox.Text = "" + Math.Cos(i);
        }

        private void BtnTan_Click(object sender, EventArgs e)
        {
            var i = float.Parse(NumbBox.Text);
            NumbBox.Text = "" + Math.Tan(i);
            //ans = floaMath.Tan(i);
        }

        private void BtnMult_Click(object sender, EventArgs e)
        {
            var i = float.Parse(NumbBox.Text);
            NumbBox.Text = "" + Math.Sin(i);
        }
    }
}
